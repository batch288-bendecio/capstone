const express = require("express");
const auth = require("../auth.js");
const orderControllers = require("../controllers/ordersControllers.js");


const router = express.Router();

router.get("/allOrders", auth.verify, orderControllers.allOrders)

router.post("/:id", auth.verify, orderControllers.orderProduct)

router.delete("/:id/received", auth.verify, orderControllers.orderReceived);

module.exports = router;