const express = require("express");
const userControllers = require("../controllers/usersControllers.js");
const auth = require("../auth.js");

const router = express.Router();

// registration route
router.post("/registration", userControllers.userReg);

// login route
router.post("/login", userControllers.loginUser);

// for checking user data (non-admin)
router.get("/userInfo", auth.verify, userControllers.userInfo);

// getProfile

// for making user an admin (admin only)
router.patch("/:id/makeAdmin", auth.verify, userControllers.makeAdmin);




module.exports = router;