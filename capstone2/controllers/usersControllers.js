const Users = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Products = require("../models/Products.js")

module.exports.userReg = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {

		if(result){
			return response.send(false)
		} else {

			let newUser = new Users({

				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				mobileNumber: request.body.mobileNumber,
				password: bcrypt.hashSync(request.body.password, 5)
			})

			newUser.save()
			return response.send(true)
		}
	})
	.catch(error => response.send(false))
}

module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {

		if(!result){
			return response.send({email: null})
		} else {

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({
					auth: auth.createAccessToken(result)
				})
			} else {

				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false));
}

module.exports.userInfo = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	
	Users.findById({_id: userData.id})
	.then(result => response.send(result))
	.catch(error => response.send(false))
}

module.exports.makeAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){

		return response.send(false)

	} else {

		const userId = request.params.id;
		const toAdmin = {isAdmin: request.body.isAdmin}

		Users.findByIdAndUpdate(userId, toAdmin)
		.then(result => {

				if(request.body.isAdmin == false) {

					return response.send(true)

				} else {

					return response.send(true)

				}
		})
		.catch(error => response.send(false))
	}
}
