const Orders = require("../models/Orders.js");
const auth = require("../auth.js");
const Products = require("../models/Products.js")
const Users = require("../models/Users.js")

module.exports.orderProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const qty = request.body.product.qty;
	
	if(!userData.isAdmin){

		return response.send(true)

	} else {

		Products.findById({_id: request.params.id})
		.then(result => {

			let newOrder = new Orders({
				userId: userData.id,
				product: [{
					productId: result.id,
					productName: result.name,
					qty: qty
				}],
				total: (qty*result.price)
			})
				
			newOrder.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false))

		})
	}
}

module.exports.allOrders = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization)

	Orders.find({})
	.then(result => {

		if(!userData.isAdmin){

			return response.send(false)

		} else {

			return response.send(result)

		}
	})
}

module.exports.orderReceived = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){

		return response.send(false)

	} else {

		Orders.findOneAndDelete({_id: request.params.id})
		.then(result => {

			if(result){

				return response.send(true)

			} else {

				return response.send(false)

			}
		})
		.catch(error => response.send(false))
	}
}