const Products = require("../models/Products.js");
const auth = require("../auth.js");

module.exports.addProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);


	if(!userData.isAdmin){

		return response.send(false)

	} else {

	Products.findOne({name: request.body.name})
	.then(result => {

			if(result){

				return response.send(false)

			} else {

				let newProducts =  new Products({

					name: request.body.name,
					description: request.body.description,
					price: request.body.price,
					qty: request.body.qty

				})

				newProducts.save()

				return response.send(true)
			}

	})
	.catch(error => response.send(false))
}
}

module.exports.allProducts = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization)

	Products.find({})
	.then(result => {

		if(!userData.isAdmin){

			return response.send(false)

		} else {

			return response.send(result)

		}
	})
	.catch(error => response.send(false))
}

module.exports.activeProducts = (request, response) => {

	Products.find({isActive: true})
	.then(result => {

		if(result){

			return response.send(result);

		} else {

			return response.send(false)
		}
	})
	.catch(error => response.send(false))
}

module.exports.specificProduct = (request, response) => {

	Products.findOne({isActive: true, _id: request.params.id})
	.then(result => {

		if(result){

			return response.send(result)

		} else {

			return response.send(false)

		}
	})
	.catch(error => response.send(false))
}

module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const productId = request.params.id;

	let updatedProduct = {

		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		qty: request.body.qty,
		isActive: request.body.isActive
	}

	Products.findOneAndUpdate({_id: productId}, updatedProduct)
	.then(result => {

		if(!userData.isAdmin){

			return response.send(false)

		} else {

			return response.send(true)

		}
	})
	.catch(error => response.send(false))
}

module.exports.updateStatus = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	const productId = request.params.id;

	let status = {isActive: request.body.isActive}

	Products.findOneAndUpdate({_id: productId}, status)
	.then(result => {

		if(!userData.isAdmin){

			return response.send(false)

		} else {

			if(request.body.isActive === false || request.body.isActive === true){

				return response.send(true)

			} else {

				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false))
}


module.exports.specificAllProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){

		return response.send(false)

	} else {

		Products.findOne({_id: request.params.id})
		.then(result => {

		if(result){

			return response.send(result)

		} else {

			return response.send(false)

		}
	})
	.catch(error => response.send(false))

	}
}

module.exports.deleteProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){

		return response.send(false)

	} else {

		Products.findOneAndDelete({_id: request.params.id})
		.then(result => {

			if(result){

				return response.send(true)

			} else {

				return response.send(false)

			}
		})
		.catch(error => response.send(false))
	}
}