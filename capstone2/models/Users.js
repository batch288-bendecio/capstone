const mongoose = require("mongoose");

const userSchema = mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "Please input your first name!"]
	},

	lastName: {
		type: String,
		required: [true, "Please input your last name!"]
	},

	email: {
		type: String,
		required: [true, "Please input your valid email!"]
	},

	password: {
		type: String,
		required: [true, "Please input your password!"]
	},

	mobileNumber: {
		type: String,
		required: [true, "Please input your mobile number!"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	}
})







const Users = mongoose.model("Users", userSchema);

module.exports = Users;

