// Other important imports
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import {BrowserRouter, Route, Routes, us} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';

// components
import NavBar from './components/NavBar.js';

// pages
import AdminDashboard from './pages/AdminDashboard.js';
import CheckOut from './pages/CheckOut.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Orders from './pages/Orders.js';
import PageNotFound from './pages/PageNotFound.js';
import ProductView from './pages/ProductView.js'
import Register from './pages/Register.js';
import Shop from './pages/Shop.js';
import UpdateProduct from './pages/UpdateProduct.js';


function App() {

  let userDetails = {};

  useEffect(()=>{

    if(localStorage.getItem('token') === null){

      userDetails = {
        id: null,
        isAdmin: null
      }

    } else {

        fetch(`${process.env.REACT_APP_API_URL}/users/userInfo`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
            id: data._id,
            isAdmin: data.isAdmin
          }
      })
    }

  }, [])

    const [user, setUser] = useState(userDetails);

  const unsetUser = () => {
  
    localStorage.clear()

  }

  return (
    
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <NavBar/>
        <Routes>

          <Route path = "/" element = {<Home/>} />

          <Route path = "/shop" element = {<Shop/>} />

          <Route path = "/login" element = {localStorage.getItem('token') === null ? <Login/> : <PageNotFound/>} />

          <Route path = "/register" element = {localStorage.getItem('token') === null ? <Register/> : <PageNotFound/>} />

          <Route path = "/logout" element = {<Logout/>} />

          <Route path = "/orders/:id" element = {<CheckOut/>} />

          <Route path = "/products/:id" element = {<ProductView/>} />

          <Route path = "*" element = {<PageNotFound/>} />

          <Route path = "/dashboard" element = {user.isAdmin ? <AdminDashboard/> : <PageNotFound/>} />

          <Route path = "/orders" element = {user.isAdmin ? <Orders/> : <PageNotFound/>} />

          <Route path = "/products/update/:id" element = {user.isAdmin ? <UpdateProduct/> : <PageNotFound/>} />

        </Routes>
      </BrowserRouter>
    </UserProvider>

  );
}

export default App;
