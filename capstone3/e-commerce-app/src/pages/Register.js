import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function Register(){

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)
	const navigate = useNavigate();

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNumber.length > 10 && email !== '' && password1 === password2 && password1.length > 5){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}

	}, [email, firstName, lastName, mobileNumber, password1, password2])

	function register(e){

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/registration`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNumber: mobileNumber,
				password: password1
			})
		})
		.then(result => result.json())
		.then(data => {

			if(!data){

				Swal2.fire({
					title: "Email has been taken",
					icon: "error",
					text: "Please use another email!"
				})

			} else {

				Swal2.fire({
					title: "Registration successfull",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				navigate('/login')
			}
		})
	}


return(

	<Container className = "col-6 mb-5 box-container">
		<Row>
			<Col>
				<Form className = "form-border p-4 ps-5" onSubmit = {e => register(e)}>

					<h1 className = "text-center text-light mb-3">Register</h1>
				    <Form.Group className="d-flex align-items-center">
				        <Form.Label className = "text-light col-6">First Name</Form.Label>
				        <Form.Label className = "text-light col-6">Last Name</Form.Label>
				       
				    </Form.Group>

				    <Form.Group className="mb-3 d-flex">
				    	 <Form.Control 
				    	 	type="text" 
				    	 	placeholder="Enter first name" 
				    	 	className = "w-75" 
				    	 	value = {firstName} 
				    	 	onChange = {e => {
				    	 		setFirstName(e.target.value)
				    	 }}/>
				    	<Form.Control 
				    		type="text" 
				    		placeholder="Enter last name" 
				    		className = "w-75 mx-5" 
				    		value = {lastName} 
				    	 	onChange = {e => {
				    	 		setLastName(e.target.value)
				    		}}
				    	/>
				    </Form.Group>

				    <Form.Group className="mb-3 col-11">
				        <Form.Label className = "text-light">Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email"
				        	value = {email} 
				    	 	onChange = {e => {
				    	 		setEmail(e.target.value)
				    		}}
				        />
				    </Form.Group>

				    <Form.Group className="mb-3 col-11">
				        <Form.Label className = "text-light">Mobile No.</Form.Label>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter mobile no."
				        	value = {mobileNumber} 
				    	 	onChange = {e => {
				    	 		setMobileNumber(e.target.value)
				    		}}
				        />
				    </Form.Group>

				    <Form.Group className="mb-3 col-11" controlId="formBasicPassword2">
				        <Form.Label className = "text-light">Password</Form.Label>
				      	<Form.Control 
				      		type="password" 
				      		placeholder="Password"
				      		value = {password1} 
				    	 	onChange = {e => {
				    	 		setPassword1(e.target.value)
				    		}}
				      	/>
				    </Form.Group>

				    <Form.Group className="mb-3 col-11" controlId="formBasicPassword2">
				        <Form.Label className = "text-light">Confirm Password</Form.Label>
				      	<Form.Control 
				      		type="password" 
				      		placeholder="Confirm password"
				      		value = {password2} 
				    	 	onChange = {e => {
				    	 		setPassword2(e.target.value)
				    		}}
				      	/>
				    </Form.Group>

				    <Button className = "btn-registration col-5 mb-3 mt-3" type="submit" disabled = {isDisabled}>
				        Register
				    </Button>

				</Form>
			</Col>
		</Row>
	</Container>

	)

}