import {Container, Row, Col, Button, Form, Modal} from 'react-bootstrap';
import ProductProps from '../components/ProductProps.js';
import {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import { faCircleUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Swal2 from 'sweetalert2';



export default function AdminDashboard(){

	const [products, setProducts] = useState([])
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [qty, setQty] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)
	const [show, setShow] = useState(false);
	const navigate = useNavigate();

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	useEffect(() => {

		if(name !== '' && description !== '' && price !== '' && qty !== ''){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}

	}, [name, description, price, qty])

	const addProducts = (e) => {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
			method: "POST",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem('token')}`,
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				qty: qty
			})
		})
		.then(result => result.json())
		.then(data => {
			
			if(!data){

				Swal2.fire({
					title: "Product already in the list",
					icon: "error",
					text: "If you want some changes in the product just go to update"
				})

			} else {

				Swal2.fire({
					title: "Added successfully",
					icon: "success",
					text: "Product has been added to the list"
				})

				handleClose()
				setName('')
				setDescription('')
				setPrice('')
				setQty('')
			}

		})
	}


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(products => {
				return (
					<ProductProps 
					key = {products._id} 
					productProp = {products} 
					onActivate = {() => {archieve(products._id, true)}} 
					onDeactivate = {() => {archieve(products._id, false)}} />
				)
			}))
		})
	})


	const archieve = (_id, productIsActive) => {

		// console.log(productIsActive)

		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/${_id}/updateStatus`, {
			method: "PATCH",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`,
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				isActive: productIsActive
			})
		})
		.then(result => result.json())
		.then(data => {

			if(productIsActive){

					Swal2.fire({
						title: "Activated successfully",
						icon: "success",
						text: `Product has been activated!`
					})

			} else {

				Swal2.fire({
					title: "Deactivated successfully",
					icon: "success",
					text: `Product has been deactivated!`
				})
			
			}

		})
	}


return(

	<Container>
		<h1 className = "fonts text-center text-light">Admin Dashboard</h1>

		<Row className = "mb-3">
			<Col className = "col-6 d-flex justify-content-end">
				<Button className = "btn-add p-3" onClick = {handleShow} >Add Product</Button>

				<Modal show={show} onHide={handleClose} className = "modal-container">
				        <Modal.Header className = "d-flex justify-content-center">
				          <Modal.Title>Add Products</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				          <Form>
				            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1">
				              <Form.Label>Product Name</Form.Label>
				              <Form.Control
				                type="text"
				                placeholder="Enter product name"
				                autoFocus
				                value = {name} 
				    	 		onChange = {e => {
				    	 			setName(e.target.value)
				    			}}
				              />
				            </Form.Group>
				            <Form.Group
				              className="mb-2"
				              controlId="exampleForm.ControlTextarea1"
				            >
				              <Form.Label>Description</Form.Label>
				              <Form.Control 
				                as = "textarea"
				                placeholder="Enter description"
				                value = {description} 
				              	onChange = {e => {
				              		setDescription(e.target.value)
				              	}}
				              />
				            </Form.Group>

				            <Form.Group className="mb-2" controlId="price">
				                <Form.Label>Price</Form.Label>
				              	<Form.Control 
				              		type="number" 
				              		placeholder="Enter price"
				              		value = {price} 
				            	 	onChange = {e => {
				            	 		setPrice(e.target.value)
				            		}}
				              	/>
				            </Form.Group>

				            <Form.Group className="mb-2" controlId="quantity">
				                <Form.Label>Quantity</Form.Label>
				              	<Form.Control 
				              		type="number" 
				              		placeholder="Enter quantity"
				              		value = {qty} 
				            	 	onChange = {e => {
				            	 		setQty(e.target.value)
				            		}}
				              	/>
				            </Form.Group>
				          </Form>
				        </Modal.Body>
				        <Modal.Footer className = "d-flex justify-content-evenly">
				        	<Button className = "col-5 mb-3 mt-3 btn-cancel" onClick = {handleClose}>
				         	    Cancel
				         	</Button>
				         	<Button className = "col-5 mb-3 mt-3 btn-addProducts" onClick = {e => addProducts(e)} disabled = {isDisabled}>
				         	    Add
				         	</Button>
				        </Modal.Footer>
				      </Modal>
			</Col>

			<Col className = "col-6 d-flex justify-content-start">
				<Button className = "btn-user-orders p-3" as = {Link} to = "/orders" >Show User Orders</Button>
			</Col>

		</Row>

		<Row className = "text-center text-light bg-dark rounded-6 opacity-75 p-3 dashboard">
			<Col>
				<h4>Product Name</h4>
			</Col>

			<Col>
				<h4>Description</h4>
			</Col>

			<Col>
				<h4>Quantity</h4>
			</Col>

			<Col>
				<h4>Prices</h4>
			</Col>

			<Col>
				<h4>Status</h4>
			</Col>


			<Col>
				<h4>Action</h4>
			</Col>


		</Row>

		<Row className = "mb-3">
			<Col>
				{products}
			</Col>
		</Row>
	</Container>	

	)
}