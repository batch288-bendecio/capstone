import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import {useContext, useEffect} from 'react';



export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		setUser({
			id: null,
			isAdmin: null
		});
	}, [])

	return(

		<Navigate to = "/login" />

		)
}