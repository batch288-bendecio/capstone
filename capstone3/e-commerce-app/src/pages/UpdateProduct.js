import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate, useParams} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';


export default function UpdateProducts(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [qty, setQty] = useState('');
	const [isActive, setIsActive] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)
	const navigate = useNavigate();

	const { id } = useParams()

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/${id}`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
			setQty(data.qty)
			setIsActive(data.isActive)
		})
	}, [])

	function updateProducts(e){

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts/${id}/update`, {
			method: "PATCH",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem('token')}`,
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				qty: qty,
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(data => {
			
			if(!data){

				Swal2.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

			} else {

				Swal2.fire({
					title: "Updated successfully",
					icon: "success",
					text: "Product has been updated"
				})

				navigate('/dashboard')
			}

		})
	}


return(

	<Container className = "col-6 mb-5 box-container">
		<Row>
			<Col>
				<Form className = "form-border p-4 ps-5" onSubmit = {e => updateProducts(e)}>

					<h1 className = "text-center text-light mb-3">Update Product</h1>

				    <Form.Group className="mb-3 col-11">
				        <Form.Label className = "text-light">Product Name</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	placeholder="Enter product name"
				        	value = {name} 
				    	 	onChange = {e => {
				    	 		setName(e.target.value)
				    		}}
				        />
				    </Form.Group>

				    <Form.Group className="mb-3 col-11">
				        <Form.Label className = "text-light">Description</Form.Label>
				        <Form.Control 
				        	as = "textarea"
				        	placeholder="Enter description"
				        	value = {description} 
				    	 	onChange = {e => {
				    	 		setDescription(e.target.value)
				    		}}
				        />
				    </Form.Group>

				    <Form.Group className="mb-3 col-11" controlId="price">
				        <Form.Label className = "text-light">Price</Form.Label>
				      	<Form.Control 
				      		type="number" 
				      		placeholder="Enter price"
				      		value = {price} 
				    	 	onChange = {e => {
				    	 		setPrice(e.target.value)
				    		}}
				      	/>
				    </Form.Group>

				    <Form.Group className="mb-3 col-11" controlId="quantity">
				        <Form.Label className = "text-light">Quantity</Form.Label>
				      	<Form.Control 
				      		type="number" 
				      		placeholder="Enter quantity"
				      		value = {qty} 
				    	 	onChange = {e => {
				    	 		setQty(e.target.value)
				    		}}
				      	/>
				    </Form.Group>

				     <Form.Group className="mb-3 col-11" controlId="isActive">
				        <Form.Label className = "text-light">Status</Form.Label>
				      	<Form.Control 
				      		type="text" 
				      		placeholder="Enter status"
				      		value = {isActive} 
				    	 	onChange = {e => {
				    	 		setIsActive(e.target.value)
				    		}}
				      	/>
				    </Form.Group>

				    <Button className = "btn-registration col-5 mb-3 mt-3" type="submit">
				        Save
				    </Button>

				</Form>
			</Col>
		</Row>
	</Container>

	)

}