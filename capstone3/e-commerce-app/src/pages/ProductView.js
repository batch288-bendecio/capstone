import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom'
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import Swal2 from 'sweetalert2';

export default function ProductView(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [qty, setQty] = useState('');
	const [price, setPrice] = useState('');

	const navigate = useNavigate();

	const { id } = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
			setQty(data.qty)
		})
	}, [])



return(

	<Container className = "view-container">
		<Row>
			<Col>
				<Card className = "card-view">
				   	<Card.Body>
				       	<Card.Title>{name}</Card.Title>
				     	<Card.Subtitle>Description:</Card.Subtitle>
				     	<Card.Text>{description}</Card.Text>
				     	<Card.Subtitle>Price:</Card.Subtitle>
				     	<Card.Text>PHP {price}</Card.Text>
				     	<Card.Subtitle>Quantity</Card.Subtitle>
				     	<Card.Text>{qty}</Card.Text>
				       	<Button variant="primary">Buy</Button>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}