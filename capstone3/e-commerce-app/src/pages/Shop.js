import {Container, Col, Row} from 'react-bootstrap';
import ShopProps from '../components/ShopProps.js';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


export default function Shop(){

	const [products, setProducts] = useState([])


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(products => {
				return (
					<ShopProps key = {products._id} shopProp = {products} />
				)
			}))
		})
	}, [])

	return(

		<Container fluid="md">
			<h1 className = "text-center text-light fonts mt-3 mb-3">Shop Wise !</h1>
			<Row className = "d-flex align-items-center justify-content-center">
				{products}
			</Row>
		</Container>

		)
}