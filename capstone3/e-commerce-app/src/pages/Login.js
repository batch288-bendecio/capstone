import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import { faEye, faEyeSlash, faEnvelope, faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// sweet alert
import Swal2 from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [eye,setEye] = useState(true);
	const [passwordType, setPasswordType] = useState("password");
	const [isDisabled, setIsDisabled] = useState(true);
	const navigate = useNavigate();
	const {setUser} = useContext(UserContext);
	


	useEffect(() => {

		if(email !== '' && password !== ''){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}

	}, [email, password])

	function login(e){

			e.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(result => result.json())
			.then(data => {

				if(data.email === null){

					Swal2.fire({
						title: "Email is not yet registered",
						icon: "error",
						text: "Please register first"
					})

					navigate("/register")

				} else if(!data){

					Swal2.fire({
						title: "Wrong password",
						icon: "error",
						text: "Please try again"
					})

				} else {

					localStorage.setItem("token", data.auth)
					userInfo(data.auth);

					Swal2.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Game Shop!"
					})

					navigate("/");
				}
			})
		}


	const userInfo = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userInfo`, {
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	const Eye = (e) => {
    if(passwordType === "password"){
        setPasswordType("text");
        setEye(true);
    }
    else{
        setPasswordType("password");
        setEye(false);
    }
}



return(

	<Container className = "col-10 col-md-8 col-lg-4 mt-5 box-container">
		<Row>
			<Col>
				<Form onSubmit = {e => login(e)}className = "form-border p-4">
					<h1 className = "text-center text-light mb-3">Login</h1>
				    <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label className = "text-light">Email address</Form.Label>
				        <Form.Control type="email" value = {email} onChange = {e => setEmail(e.target.value)} placeholder="Enter email"/>
				        <FontAwesomeIcon icon={faEnvelope} className = "envelope" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label className = "text-light">Password</Form.Label>
				      	<Form.Control type={passwordType} value = {password} onChange = {e => setPassword(e.target.value)} placeholder="Password"/>
				      	<FontAwesomeIcon icon={faLock} className = "lock" />
				      	<FontAwesomeIcon onClick = {e => Eye(e)} icon={eye ? faEyeSlash : faEye } className = "eye" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicCheckbox">
				        <Form.Check type="checkbox" label="remember password" />
				    </Form.Group>

				    <Button className = "btn-login col-5 mb-3 mt-3" type="submit" disabled = {isDisabled} >
				        Login
				    </Button>

				    <Button className = "btn-register col-5 mb-3 mt-3" type="submit" as = {Link} to = {"/register"}>
				        Register
				    </Button>
				</Form>
			</Col>
		</Row>
	</Container>

	)

}