import LandingPage from '../components/LandingPage.js';
import HighLights from '../components/HighLights.js';
import Footer from '../components/Footer.js';

export default function Home(){

	return(

	<>
	
		<LandingPage />
		<HighLights />
		<Footer />

	</>

		)
}