import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom'
import {useContext} from 'react';
import Swal2 from "sweetalert2";



export default function OrderProps(props){

	const {_id, product, total} = props.orderProp;

	const deleteOrder = () => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/${_id}/received`, {
			method: "DELETE",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(result => result.json())
		.then(data => {
			
			if(data){

				Swal2.fire({
					title: "Order Completed",
					icon: "success",
					text: `Order has been received!`
				})
			} else {

				Swal2.fire({
					title: "Something went wrong",
					icon: "error",
					text: `Please try again`
				})
			}
		})
	}


return(

			<Col className = "col-4 mx-auto mb-5">
				<Card className = "d-flex align-items-start order-container">
					<Card.Body>
												{
							product.map(products => (
								<>
							        <Card.Subtitle>Product Name:</Card.Subtitle>
							        <Card.Text>{products.productName}</Card.Text>
							        <Card.Subtitle>Quantity:</Card.Subtitle>
							        <Card.Text>{products.qty}</Card.Text>
								</>
							))
						}

						<Card.Subtitle>Total:</Card.Subtitle>
						<Card.Text>PHP {total.toLocaleString()}</Card.Text>
						<Button className = "btn-received" onClick = {deleteOrder} >Order Received</Button>
					</Card.Body>
				</Card>
			</Col>

	)

}