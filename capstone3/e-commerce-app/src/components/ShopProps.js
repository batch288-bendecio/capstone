import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom'
import {useContext} from 'react';
import UserContext from '../UserContext.js';



export default function ShopProps(props){

	const {_id, name, description, qty, price} = props.shopProp;

	const {user} = useContext(UserContext);

return(


			<Col className = "col-8 col-lg-4 col-md-6 mx-auto mb-4">
				<Card className = "d-flex align-items-start shop-container">
					<Card.Body>
						<Card.Title>{name}
						</Card.Title>
				        <Card.Subtitle className = "mt-3">Stocks:</Card.Subtitle>
				        <Card.Text className = "mb-3">{qty}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text className = "mb-3">PHP {price.toLocaleString()}</Card.Text>
					    {
					   	user.id !== null ?
				     	<Button className = "btn-shop" variant="primary" as = {Link} to = {`/orders/${_id}/`} disabled = {user.isAdmin ? true : false} >Check Out</Button>
				      	:
				      	<Button className = "btn-shop" as = {Link} to = "/login">Login in to Buy</Button>
					    }
					</Card.Body>
				</Card>
			</Col>

	)

}