import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon,
  MDBBtn
} from 'mdb-react-ui-kit';


export default function Footer(){

	return(
			
		    <MDBFooter className='text-center text-white'>
		          <MDBContainer className='p-4 pb-0'>
		            <section className='mb-4'>
		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://web.facebook.com/kinerst02' role='button'>
		                <MDBIcon fab icon='facebook-f' />
		              </MDBBtn>

		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://twitter.com/BendecioKinley' role='button'>
		                <MDBIcon fab icon='twitter' />
		              </MDBBtn>

		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://mail.google.com/mail/u/1/#inbox' role='button'>
		                <MDBIcon fab icon='google' />
		              </MDBBtn>
		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://www.instagram.com/kindotexdi/' role='button'>
		                <MDBIcon fab icon='instagram' />
		              </MDBBtn>

		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://www.linkedin.com/in/kinley-bendecio-69a5ba275/' role='button'>
		                <MDBIcon fab icon='linkedin-in' />
		              </MDBBtn>

		              <MDBBtn outline color="light" floating className='m-1' target = "_blank"  href='https://github.com/kinerst25' role='button'>
		                <MDBIcon fab icon='github' />
		              </MDBBtn>
		            </section>
		          </MDBContainer>

		           <div className='text-center pt-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
		            Copyright © 2023 Game Shop, Inc.</div>

		           <div className='text-center pb-1 pt-1 text-decoration-underline' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
		            <p>
		            	<a className='text-white mx-3' href='#bottom'>
		            	  Terms & Condition
		            	</a>
		            	<a className='text-white mx-3' href='#bottom'>
		            	  Privacy Policy
		            	</a>
		            	<a className='text-white mx-3' href='#bottom'>
		            	  Security
		            	</a>
		            </p>
		          </div>
		        </MDBFooter>

		)
}